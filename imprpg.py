#!/usr/bin/env python3
# ^ what to run if its an executable, it NEEDS to be python3
"""a text adventure game, for showcasing purposes"""
__author__ = "imp"
__credits__ = ["imp", "echo", "Jordan"]

import os.path
import sys
import pickle
import math
from collections import OrderedDict

# when we want to return specific things from data,
# for example, the sword, we use OrderedDict
# to ensure the order is the insertion order
data = {}  # we get the info from save file using global


def saveFile():  # create a new file as well as welcome the user, this also saves the game
	global data  # we make data global, which means when we modify it, it applies outside the function
	home = os.path.dirname(os.path.abspath(__file__)) + "/save.sav"  # location of save file
	# the above code gives us the directory of our script, so I'm just adding /save.sav on the end to save it there
	if len(data) == 0:
		try:
			with open(home, "xb+") as f:  # create the save file if its not there
				data = {}  # just created, so our dict is empty
				pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)
		# we're not actually using the file at all, we just want to see if we can create it
		except (OSError, IOError):  # if we can't create it, open it
			with open(home, 'rb+') as f:
				data = pickle.load(f)  # load the data

	if "user" not in data:  # user in data is not present we haven't created a character
		data["user"] = input(
			"Welcome to ImpRPG, this is short and still in active development.\n\nEnter a username:\n> ")

		user_class = input("\nPlease select a class:\n  1. Warrior\n  2. Ranger\n  3. Wizard\n\n> ")

		# horrible hardcoding
		while user_class != "1" and user_class != "2" and user_class != "3":
			user_class = input("\nInvalid option!\n> ")

		if "inventory" not in data:
			data['inventory'] = {}

		if user_class == "1":
			user_class = "warrior"
			data = insertDictPaths('sword', {
				'where'   : ['inventory'],
				'desc'    : 'This sharp blade could cut something in two!',
				'names'   : ['sword', 'longsword'],
				'weapon'  : ['chop', "slash", "stab"],
				'weight'  : 3,
				'takeable': False
			}, ['inventory'], data)

		if user_class == "2":
			user_class = "ranger"
			data = insertDictPaths('bow', {
				'where'   : ['inventory'],
				'desc'    : 'This bow can launch powerful arrows!',
				'names'   : ['bow', 'shortbow'],
				'weapon'  : ['shoot', 'fire'],
				'weight'  : 2,
				'takeable': False
			}, ['inventory'], data),
			data = insertDictPaths('arrows', {
				'where'   : ['inventory'],
				'desc'    : 'A bundle of arrows.',
				'names'   : ['arrows', 'quiver', 'bundle of arrows'],
				'weapon'  : [],
				'weight'  : 3,
				'takeable': False
			}, ['inventory'], data)

		if user_class == "3":
			user_class = "wizard"
			data = insertDictPaths('wooden staff', {
				'where'   : ['inventory'],
				'desc'    : 'A simple wooden staff.',
				'names'   : ['wooden staff', 'staff'],
				'weapon'  : ['bash', 'smack'],
				'weight'  : 2,
				'takeable': False
			}, ['inventory'], data)
			data = insertDictPaths('magic tome', {
				'where'   : ['inventory'],
				'desc'    : 'Where you store all your magic spells!',
				'names'   : ['magic tome', 'tome', 'magic book'],
				'weapon'  : [],
				'weight'  : 2,
				'takeable': False
			}, ['inventory'], data)

		data["class"] = user_class

		print("\nYour name is {} and you're a {}".format(data["user"], data["class"]))

		conf = input("Confirm?\n> ")
		if not conf.startswith("y"):
			input("You have not confirmed your character.")
			sys.exit()

		print("\nProgress is saved whenever you take an action,\n"
			"upon restarting the script the latest summary message is displayed.\n")

		recentUpdate("You just started your adventure.")

	with open(home, 'wb') as file:
		pickle.dump(data, file, protocol=pickle.HIGHEST_PROTOCOL)


def recentUpdate(text=None):  # recent updates, also saves the game
	if "recent" not in data:  # create the key if it doesn't exist
		data["recent"] = []

	if text:  # if its not a string we just return the latest update rather than appending
		data["recent"].append(text)  # add input to recent key

	saveFile()  # save our progress
	return data["recent"][-1]  # return the latest entry


def welcomeMsg():  # welcome the player
	saveFile()  # run save file
	print("Welcome {}, you're a {}!\n\nLatest update:\n{}".format(data["user"], data["class"], recentUpdate()))
	# welcome msg

	if "step" not in data:  # creating keys as I go rather than constantly remake new save files
		data["step"] = {}


def startGame():  # part one
	if "start" not in data["step"]:  # this only triggers if we haven't done part one
		# otherwise this function does nothing, the same principle applies to the other parts
		# that way, we can just call them one after the other at the end.
		start = "\n".join([
			"\n\nYou awake in a dimly lit room, unsure about your surroundings.",
			"Your trusty {} at your side, you're prepared for any threat",
			"which may emerge. You see two doors at the opposite end of the room.",
			"One left, one right.",
			"What do you do?"
		]).format(list(OrderedDict(data["inventory"]))[0])
		print(start)

		while True:
			inp = input("\n> ")
			options = ["open", "sum"]
			com = commandList(inp, options)
			if com == "sum":
				print(start)

			if com == "open":
				if "open door" in inp:
					print("open which door?")

				elif inp == "door" or "left" not in inp and "right" not in inp:
					print("open what?")

				else:
					if "left" in inp:
						data["step"]['start'] = "left"
						recentUpdate("You went through the left door.")
						return partTwo()
					else:
						print("It seems to be jammed.")


def partTwo():
	global data
	if "p2" not in data["step"]:  # check if we should even be here
		if data["step"]["start"] == "left":  # if we picked the left door
			# start message, displays on rerun of script
			retmsg = "\n".join([
				"\n\nYou go through the left door, the next room looks pretty bare.",
				"There's a table in the center of the room, with a clay mould, a ",
				"bunsen burner and some liquid in a vial.",
				"Next to the bunsen burner is an open dusty book.",
				"\nAt the opposite end of the room is a door with a padlock on it.",
				"On the nearby wall is a key attached to a chain.",
				"\nWhat do you do?\n"
			])
			print(retmsg)

			if "options" not in data:  # 'options' will store our objects and actions
				data['options'] = {}

			if 1 not in data['options']:  # create objects and valid actions
				data["options"][1] = {
					'objects': {},
					'actions': ["use", "grab", "take", "open", "sum"]
				}
				data = itemCreate('door', {
					'where'   : ['options', 1, 'objects'],
					'desc'    : 'A thick wooden door.',
					'names'   : ['door', 'wooden door'],
					'weapon'  : [],
					'weight'  : 999,
					'takeable': False
				}, data)

				data = itemCreate('padlock', {
					'where'   : ['options', 1, 'objects'],
					'desc'    : 'A sturdy padlock, blocking the door from being opened.',
					'names'   : ['lock', 'padlock'],
					'weapon'  : [],
					'weight'  : 999,
					'takeable': False
				}, data)

				data = itemCreate('key', {
					'where'   : ['options', 1, 'objects'],
					'desc'    : 'A key attached to a chain.',
					'names'   : ['key'],
					'weapon'  : [],
					'weight'  : 999,
					'takeable': False
				}, data)

				data = itemCreate('clay', {
					'where'   : ['options', 1, 'objects'],
					'desc'    : 'Some moulding clay.',
					'names'   : ['clay', 'clay mould', 'moulding clay'],
					'weapon'  : [],
					'weight'  : 1,
					'takeable': True
				}, data)

				data = itemCreate('vial', {
					'where'   : ['options', 1, 'objects'],
					'desc'    : 'A vial of mysterious liquid',
					'names'   : ['vial', 'liquid', 'vial of liquid', 'liquid vial'],
					'weapon'  : [],
					'weight'  : 1,
					'takeable': True
				}, data)

				data = itemCreate('dusty book', {
					'where'   : ['options', 1, 'objects'],
					'desc'    : "\n".join([
						"The book is opened to a page titled: \"Creating And Using Moulds: The basics\"",
						"Below the title is a paragraph circled in cracking, red ink, at least you hope it's ink.",
						"It reads:",
						"First, take your moulding clay and shape it around your desired object, creating an imprint.",
						"Next, pour a vial of 238 quick-dry liquid into the mould, wait two to three minutes.",
						"Once hardened into a paste-like substance, heat at 200 degrees celsius for 30 seconds.",
						"After heating; break away clay to have a hardened, strong mimic of the object you moulded."
					]),
					'names'   : ['book', 'dusty book', 'old book'],
					'weapon'  : [],
					'weight'  : 1,
					'takeable': False
				}, data)

				data = itemCreate('bunsen burner', {
					'where'   : ['options', 1, 'objects'],
					'desc'    : 'Fire at your convenience',
					'names'   : ['burner', 'fire', 'bunsen'],
					'weapon'  : [],
					'weight'  : 1,
					'takeable': False
				}, data)

			loc = ['options', 1, 'objects']
			actions = ["options", 1, "actions"]
			while True:  # infinite loop so user can give constant commands
				inp = input("\n> ")  # the user input
				com = commandList(inp, actions, loc)  # get commands, passing our commands for this part also
				if com == "sum":  # if we got sum, print retmsg
					print(retmsg)
				if com == "take" or com == "grab":  # if we got take or grab, run take()
					print(take(inp, loc))

				if com == "use":  # if we got use, return use(), check if we have fake key first though
					# code needs revising, less hardcoding ty
					if "key" in inp and "key" in inv():
						if "door" in inp or "lock" in inp:
							input("You unlock the door, going through it, you see a bright white light, you can't see "
								"a thing!\n\nThat's all there is for now, infrastructure is mostly done so expect "
								"more.")
							sys.exit()

					print(use(inp, loc))


def commandList(inp, actions=None, where=None):  # standard universal commandList
	# if we passed actions that means we have extra commandList to return to the user
	check = dictPaths(data, actions, True)  # see if we have those stored in data
	# possible commandList
	command_list = ["help", "inspect", "inventory", "recent"]

	if check and type(check) == list:  # if data exists and it's an array
		actions = check

	else:  # if it's not in data see if actions is a list, then extend command_list
		if type(actions) == list:
			command_list.extend(actions)

	if type(actions) == list:
		for i in actions:  # if user input is in actions, return the action
			if inp.startswith(i):
				return i

	if inp.startswith("help"):  # if help then return all commandList, sorted
		print(", ".join(sorted(command_list)))

	elif inp.startswith("rec"):  # if recent show latest update
		print(recentUpdate())

	elif inp.startswith("inv") or inp == "ls":  # if ls or inv, return inv()
		print(inv())

	elif inp.startswith("inspect"):  # if inspect return inspect
		print(inspect(inp, where))

	else:
		return print("I don't understand")


def dictPaths(start_data, where, start=True):  # this lets us search nested dictionaries
	new_data = dict(start_data)  # set new_data as start data
	if start:  # if start is true, it means we haven't started recursion yet
		where = list(where)  # just creating a duplicate of where so we aren't modifying the var we passed into it
		where.reverse()  # if so reverse the array since we need to search backwards
		if len(where) == 0:  # if you passed nothing, just return start_data
			return new_data

	if type(start_data) != dict or type(start) != bool:
		raise Exception("dictPathsErr")

	if type(where) == list:  # if we have a list, we can start searching
		try:  # try setting new_data as the key of new_data[last element of where]
			new_data = new_data[where.pop()]  # new_data is the key of new_data[last element of where]
		# ^ this also deletes the last element of where, so we can use recursion
		except KeyError:  # if it doesnt exist, you die
			return False

		if len(where) == 0:  # if the length is 0 we can return the result (since we've popped everything off)
			return new_data  # return the data from the last element of where
		# say where was ["foo", "bar"], this effectively returns data['foo']['bar']

		else:  # if the len isn't 1 we need to start doing recursion
			return dictPaths(new_data, where, False)  # return this function, causing recursion
	# this will repeat until we have a length of 1 in where, at that point, we return the data
	else:
		raise Exception("dictPathsErr")


def insertDictPaths(inp_name, inp_data, where, dict_to_ins, start=True):  # insert into nested dictionaries
	# NOTE: this doesnt change the dictionary you pass, I'm not sure how to do that without making it global, instead
	# it returns the dictionary you passed, with its shiny new insert. In order to update the data you passed set
	# the data you passed to the result of this function (it will raise an exception if something bad happens)
	where = list(where)  # just creating a duplicate of where so we aren't modifying the var we passed into it
	if type(inp_name) != str or type(inp_data) != dict or type(where) != list or type(start) != bool or type(dict_to_ins) != dict:
		# ^ error checking
		raise Exception("insertDictPathsErr")

	if start:  # if we haven't started recursion (when you call this start should be True)
		if len(where) == 0:  # if the user passed an empty array
			dict_to_ins[inp_name] = inp_data  # no need to look up data layers, just insert our data
			return dict_to_ins  # return the updated dict

		new_data = dictPaths(dict_to_ins, list(where), True)  # get the deepest layer of data using dictPaths
		if new_data is not False:  # if new_data returns a result
			# ^ just doing 'if new_data:' DOES NOT WORK. I don't know why. Maybe when the dict is empty?
			new_data[inp_name] = inp_data  # insert the data we want to insert

			if len(where) == 1:  # if we only planned to do it once
				dict_to_ins[where[0]] = new_data  # insert our data into the dict
				return dict_to_ins  # return the updated data

			else:  # we need to start recursion if we have a bigger array
				return insertDictPaths(inp_name, new_data, where, dict_to_ins, False)
		# ^ we're replacing inp_data with our current progress on the layers instead
		# since we don't need inp_data anymore, and it lets us pass dicts
		else:  # you messed up
			raise Exception("insertDictPathsErr")
	else:  # we've started recursion
		updated_data = dictPaths(dict_to_ins, list(where)[:-1:], True)

		assert updated_data is not False
		#  ^updated_data is the next layer of the dict (we're passing where but NOT the last element)
		# ^ we're not passing the last element since we need it to set the key
		updated_data[where.pop()] = inp_data  # insert our progress into this layer, also pop where since we don't
		# need the last element anymore
		# ^ we're essentially replacing all the previous layers we've been through with our new, up-to-date, data
		if len(where) == 0:  # if we've popped everything we have no more recursion left to do
			return updated_data  # return the updated data
		else:  # if we're not finished, pass our current progress onto the next call of this function
			return insertDictPaths(inp_name, updated_data, where, dict_to_ins, False)


def delDictPaths(key, where, remove_data, start=True):  # delete keys from nested dictionaries
	where = list(where)  # duplicate where so we dont modify the var we might pass into it
	if start:  # should be true on first call
		if len(where) == 0:  # if the len of where is 0 (e.g. empty dict)
			try:  # does the key exist in the first layer?
				remove_data[key]
			except KeyError as e:  # if it doesnt exist, raise error
				raise e

			del remove_data[key]  # delete the data
			return remove_data  # return updated data

		new_data = dictPaths(remove_data, list(where), True)
		# using dictPaths, get the deepest layer
		if new_data is not False:  # if not false then it exists

			try:  # does the key we want to delete exit?
				new_data[key]
			except KeyError as e:
				raise e

			del new_data[key]  # delete it

			if len(where) == 1:  # if the len of where was only 1 then we're done
				return remove_data

			else:  # we're not done, start recursion
				return delDictPaths(new_data, where, remove_data, False)

		else:  # something has gone terribly wrong
			raise Exception("delDataErr")
	else:  # recursion
		# get new data, up one layer (we're just not passing the last element of where)
		updated_data = dictPaths(remove_data, list(where)[:-1:])
		assert updated_data is not False  # if there is an assertion error something is wrong
		updated_data[where.pop()] = key  # replace old key (and its layers) with our updated data
		if len(where) == 0:  # if len is 0 then we're done, return data
			return updated_data
		else:  # we're not done, do the same process again
			return delDictPaths(updated_data, where, remove_data, False)


def searchDict(key, search, index=None, con_data=None, start=True):  # usage: searchDict('key', data)
	if len(key) == 0:  # if they passed an empty key (e.g. looking for nothing), return the data they wanted to search
		return search
	if start:  # if we haven't started recursion
		for i in list(search):  # go through each element of the data they're searching (the keys)
			if i == key:  # if your search matches a key on the top-level
				return search[i]  # return what you wanted
			else:  # update con_search to be the next layer of search (we're searching back to front in layers)
				con_search = search[i]

		return searchDict(key, search, -1, con_search, False)  # this has to be outside the for loop
	# so we get the final, updated value
	else:  # recursion baby
		if search == con_data:  # if the search data is the same as con_data
			#  ^ we reset con_data to search data if the layer we're searching came to an end and we didn't get what we wanted.
			try:
				list(search)[index]  # try another layer
			except IndexError:  # index error? we've searched everything, sorry pal
				return False

			con_data = search[list(search)[index]]  # update con_data to be another layer
			return searchDict(key, search, index, con_data, False)  # call this function again with our updated layer

		if type(con_data) != dict or len(list(con_data)):  # if con_data is empty this layer has nothing to search through
			# ^ THIS ORDER IS VERY IMPORTANT
			index += 1  # update index so we can pick another layer
			# v we're also setting con_data to search (just passing search instead), which will make us pick another layer
			return searchDict(key, search, index, search, False)  # call this function again with our updated index

		for i in list(con_data):  # we have a layer we want to search through, lets go through the keys
			if i == key:  # the key in con_data matches the key you want to find
				return con_data[i]  # return the data
			else:  # update con_data to go a layer deeper
				con_data = con_data[i]
		return searchDict(key, search, index, con_data, False)  # call again, we need to keep searching


def itemCreate(what, item_data, inp_data):  # create items, duh
	assert type(inp_data) == dict  # make sure we're inserting into a dict
	item_data = dict(item_data)  # duplicate the item+data we're passing, making a dict

	# disgusting checks
	if 'where' not in item_data or 'desc' not in item_data or 'names' not in item_data or 'weapon' not in item_data or 'weight' not in item_data or 'takeable' not in item_data:
		raise Exception("Missing required keys in itemCreate:\nwhere\ndesc\nnames\nweapon\nweight\ntakeable")

	if dictPaths(inp_data, item_data['where']) is not False:  # check insertion location is valid
		saveFile()  # save the game, then return inserted data, creating the item
		return insertDictPaths(what, item_data, item_data['where'], inp_data, True)

	else:  # you messed up
		raise Exception("itemCreatePathErr")


def take(inp, location):  # take item from location and place into inv
	location = list(location)
	global data
	if type(inp) != str or type(location) != list:
		raise Exception("takeErr")

	inp = inp.split(" ")  # split it up to examine the options
	if len(inp) > 2:
		inp[1] = " ".join(inp[1::])  # inp[1] becomes inp[1] + every index after it
	# for example, ["one", "two", "three", "four"] becomes ["one, "two three four", "three", "four"]

	if len(inp) <= 1:  # if its less or equal to 1 they're trying to take nothing
		return "Take what?"

	inp = inp[1]  # set inp to the option the user entered, this is just for easier use
	if inp in data['inventory']:  # if inp is in data that means you already have it you idiot.
		return "You already have {} in your bag.".format(inp)

	for k, v in data['inventory'].items():  # check if item is in your inventory, looking at alternate names
		if 'names' in v:
			if inp in v['names']:
				return "You already have {} in your bag.".format(inp)

	location.append(inp)  # append the name of the item to location so we can the item data
	item = dictPaths(data, list(location))  # get the item data
	if item:  # if item exists (dictPaths returns false if it doesn't)
		if not item['takeable']:  # if takeable is false you cannot take it (try and take a door? not happening)
			return "You can't take {}.".format(inp)

		check = weightCheck(item['weight'])  # check to see if we don't get overloaded
		if check:  # if we wont be overloaded
			item['where'] = ['inventory']  # move the location to our inv
			# (this doesn't move the item itself we're just modifying values)
			data = delDictPaths(inp, list(location)[:-1:], data, True)  # delete the item from its old location
			# ^ we're not passing the last element of location since that is invalid
			data = insertDictPaths(inp, item, item['where'], data, True)  # put the item into our inventory
			saveFile()  # save the game
			recentUpdate("You took {}.".format(inp))
			return "You take {} and put it into your bag.".format(inp)  # retmsg

		else:  # you fat ass, can't even take this item?
			return "You can't take {}, that's too heavy.".format(inp)
	else:  # triggers if the item doesn't exist.
		return "You can't take {}.".format(inp)  # ret msg


def cmdOpen(inp, options):  # needs deleting/revising, old code
	if type(options) != list:
		raise Exception("openListError")

	if type(inp) != str:
		raise Exception("openStrError")

	inp = inp.split(" ")
	if len(inp) != 2:
		return False

	if inp[1] in options:
		saveFile()
		return options[options.index(inp[1])]

	else:
		return False


def weightCheck(inp=0):  # see if we get overloaded, used for both take() and inv()
	try:  # if weightCheck is something like (False), we just set the check value to 0
		to_check = int(inp)
	except ValueError:
		to_check = 0

	total_weight = 0
	if type(inp) == int:
		to_check = inp

	for k, v in dictPaths(data, ['inventory'], True).items():  # go through players inv
		if v['where'] != ['inventory']:  # if the item location set is not our inv, dev error
			raise Exception("ItemMisplace")  # throw exception

		total_weight += v['weight']  # the total weight to check
		if total_weight > 50:  # if this triggers, dev error
			raise Exception("ItemOverload")  # throw exception

	total_weight += to_check  # add the check value to total weight
	if total_weight > 50:  # if its over 50 then return false (used for picking up items)
		return False
	else:
		return total_weight  # no value errors, good to go


def inv():  # return the players inventory
	check = weightCheck()
	if check:  # if our inventory isn't over the weight limit
		items = dictPaths(start_data=data, where=['inventory'])
		table_data = {}  # where we're putting our dict data for the table
		if items:  # if items exists (do we have an inventory?)
			for k, v in items.items():  # go through items in our inventory
				table_data[k] = v['weight']  # put every item and its weight into table_data

			titles = ['Item', 'Weight']  # display titles
			spaces = [len(x) for x in titles]  # the spaces needed
			for k, v in table_data.items():  # update spaces needed for each item name
				if len(str(k)) > spaces[0]:  # if the key length is higher than the spaces for this column, update it
					spaces[0] = len(k)
				if len(str(v)) > spaces[0]:  # if the value length is higher than the spaces for this column, update it
					spaces[1] = len(v)

			def spacingStrings(words, spaces, sep):  # func for spacing strings (so everything is aligned)
				logic = []  # we're putting our spaces into here
				for x, y in zip(spaces, words):  # go through the spaces we passed (these are numbers)
				# also go through the words we are passing, e.g. ['foo', 'bar']
					logic.append(" " * math.floor(x - len(str(y))))  # append the spaces we need
					# maths:  the number of spaces minus the length of the word we're passing

				ret_str = []  # we're putting our words with the spaces here
				for a, b in zip(words, logic):  # go through the words and the spaces we have so far
					ret_str.append(str(a) + str(b))  # append the word with its spaces (this ensures its aligned)

				# ret_str would look something like this:
				# ["foo  ", "bar  "]
				# we need it to be:
				# foo   | bar
				# so we then join eerything with our seperator
				return sep.join(ret_str)

			ret_ar = []  # our return stirng, this will contain our full table
			sep = " \u2503 "  # our seperator, it looks similar to '|'
			ret_ar.append(spacingStrings(titles, spaces, sep))  # our titles
			for x, y in table_data.items():  # construct the rest of the data
				ret_ar.append(spacingStrings([x, y], spaces, sep))

			ret_ar.append(f"\nTotal Weight: {weightCheck()}/50")  # get the total weight
			return "\n".join(ret_ar)  # return fancy table!
		else:
			raise KeyError("No Inventory")  # you don't have an inventory, this is bad
	else:  # you are overweight, this shouldn't ever happen
		raise Exception("invOverload")


def inspect(item, where=None):  # inspect items
	if type(item) != str:  # make sure you passed a string
		raise Exception("invLookErr")
	item = item.split(" ")  # split up all the words

	if len(item) == 1 or item[1] == "":  # checks for valid inspect
		return "Inspect what?"

	if len(item) > 2:  # if it's something like 'inspect big ass book', join everything after inspect
		item = " ".join(item[1::])  # item is item[1] + every index after it

	else:
		item = item[1]  # if the len is 2 then just set item to item[1], no need to join anything

	try:  # try and see if the item is in our inventory
		item_data = data['inventory'][item]  # the item itself

	except (KeyError, IndexError):  # if there is an index error, we try find alt names in our inv
		# find alternate names, also grab the item data
		for k, v in data['inventory'].items():
			item_data = 0
			if 'names' in v:
				if item in v['names']:
					item_data = data['inventory'][k]
					item = k
					break

	if type(item_data) != dict and where is not None:  # if item data is not a dict we don't have it in our inventory
		where = list(where)  # duplicate where, not messing with passed vars
		search_data = dictPaths(data, where)  # get location data
		try:  # see if the item exists in the location, also find alternate names
			item_data = search_data[item]
		except KeyError:
			# find alternate names, also grab the item data
			for k, v in search_data.items():
				item_data = 0
				if 'names' in v:
					if item in v['names']:
						item_data = search_data[k]
						item = k
						break

	if type(item_data) != dict:  # item data would be 0 if there is no item
		return "You can't inspect that."

	if "desc" not in item_data or len(item_data['desc']) <= 1:  # if the item has no desc
		return "Item: {}.".format(item_data['names'][0])  # generic return

	else:
		saveFile()  # save the game
		ret_ar = [  # template of how we return data
			"Name: {}\n",
			"Description: {}\n",
			"In Inventory: {}\n"
		]
		in_inv = False
		if 'inventory' in item_data['where']:
			ret_ar.append('Weight: {}'.format(item_data['weight']))
			in_inv = True

		return "\n".join(ret_ar).format(item, item_data['desc'], in_inv)


def use(inp, where=None):  # use x on y
	if inp.strip() == "use":  # syntax check
		return "Use what on what?"
	inp = inp.replace("use ", "")  # delete use, we don't need it
	inp = inp.split(" on ")  # get the two items
	if len(inp) != 2:  # syntax check
		if len(inp) < 2:
			return "Use {} on what?".format(inp[0])
		else:
			return "Use what on what?"

	for i in inp:  # remove whitespace
		inp[inp.index(i)] = i.strip()

	return recipeList(inp, where)  # where the magic happens
	# ^ use() and recipeList() are essentially just one function, I should merge


def recipeList(inp, where=None):
	global data  # data needs to be global since we're modifying

	# add recipes to the list for new item combinations
	recipe_list = [
		{
			'in' : ['example', 'layout'],
			'out': ["item_name", {'item_data': 'here'}],
			'del': ['which of in', 'needs deleting?']
		},
		{
			'in' : ["clay", "key"],
			'out': ['moulded clay', {
				'where'   : ['inventory'],
				'desc'    : "Clay with an imprint of a key.",
				'names'   : ["moulded clay", "clay", "mould"],
				'weapon'  : [],
				'weight'  : 1,
				'takeable': False
			}],
			'del':["clay"]  # only delete clay since key needs to stay
		},
		{
			'in' : ["moulded clay", "vial"],
			'out': ['clay with mysterious liquid', {
				'where'   : ['inventory'],
				'desc'    : "Some moulded clay filled with mysterious liquid, perhaps it needs heating up?",
				'names'   : ["moulded clay", "clay", "mould"],
				'weapon'  : [],
				'weight'  : 1,
				'takeable': False
			}],
			'del': ["moulded clay", "vial"]
		},
		{
			'in' : ["clay with mysterious liquid", "bunsen burner"],
			'out': ["fake key", {
				'where'   : ['inventory'],
				'desc'    : "A fake clay made from hardening liquid.",
				'names'   : ['fake key', 'key'],
				'weapon'  : [],
				'weight'  : 1,
				'takeable': False
			}],
			'del': ["clay with mysterious liquid"]
		}
	]

	assert len(inp) == 2  # assert that we only have 2 items
	check = dictPaths(data, ['inventory'])  # fetch inventory data
	assert check is not False  # make sure we have an inventory
	ind_check = {}  # we're inserting item names and indicies here
	for i in range(0, len(inp)):  # go through each item

		for k, v in check.items():  # find alternate item names within your inventory.
			if 'names' in v:
				if inp[i] in v['names']:
					inp[i] = k

		# if the item is not in our inventory, look elsewhere
		if inp[i] not in check:  # this needs to be in the loop since we have more than 1 item to check
			if where:  # only triggers if we don't have a falsey value (e.g. we didn't pass where or we pass a falsey value)
				where_data = dictPaths(data,
					where)  # check at another location (which we might've passed into this func)
				for k, v in where_data.items():  # alternate item names
					if 'names' in v:
						if inp[i] in v['names']:
							inp[i] = k

				if where_data is False or inp[i] not in where_data:  # doesn't exist
					return "You can't do that."
			else:  # we didn't pass a location, we can't do anything
				return "You can't do that."

		# enumerate means we have both the index and the value
		# add the item name and its index in recipe list to ind_check
		for x, y in enumerate(recipe_list):  # appending the ind since we're calling recipe_list later
			if inp[i] in y['in']:  # if the item (the name) is in the recipe list (possible recipes)
				if inp[i] in ind_check:  # if the item is in ind_check, we append the index of it (where it is in recipe list)
					ind_check[inp[i]].append(x)
				else:  # if the item is not in ind_check, we want to create the key for it and append the recipe index
					ind_check[inp[i]] = [x]

	if len(ind_check) != len(inp):  # if the lengths don't match we don't have a possible recipe
		return "You can't do that"

	item_ind = []  # indices of the items
	for k, v in ind_check.items():  # go through ind_check, add all our indicies to it
		item_ind.extend(v)  # extend prevents multi-dimensional arrays

	item_ind = list(set([x for x in item_ind if item_ind.count(x) > 1]))  # find duplicates, only have those in the list
	if len(item_ind) > 1:  # if we have more than 1 that means we have multiple recipe requirements met
		# shouldn't have duplicate recipes
		raise Exception("Duplicate Recipes")

	if len(item_ind) == 0:  # you have valid items for recipes, but none of them are in the same recipe
		return "You can't do that, that doesn't make sense."

	item_ind = item_ind[0]  # saves some chars, we only have 1 item in the array
	item_data = recipe_list[item_ind]['out']  # get our item data
	itemCreate(item_data[0], item_data[1], data)  # create the item
	to_delete = recipe_list[item_ind]['del']  # get the items we need to delete
	for i in to_delete:  # delete the items, at least try to
		try:  # try deleting from both locations
			data = delDictPaths(i, ['inventory'], data)
		except KeyError:
			if where is not None:
				data = delDictPaths(i, where, data)
			else:
				raise Exception("TakeErr")

	# update what they did on their log, this also saves the game
	recentUpdate("You used {} on {} and acquired {}".format(inp[0], inp[1], item_data[0]))
	return "You used {} on {}, you now have {} in your bag.".format(inp[0], inp[1], item_data[0])


welcomeMsg()
startGame()
partTwo()
